use super::token::SyntaxToken;

#[derive(Debug, Clone, PartialEq)]
pub struct Node<T> {
    children: T,
    token: SyntaxToken,
}
impl<T> Node<T> {
    pub fn new(_node: T, _token: SyntaxToken) -> Node<T> {
        Node {
            children: _node,
            token: _token,
        }
    }
}
