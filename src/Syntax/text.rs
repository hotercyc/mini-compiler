use std::fmt::{format, Display};

// 用来指向令牌位置;
#[derive(Debug, Clone, PartialEq)]
pub struct SourceLocation {
    //行;
    pub line: usize,
    //列;
    pub column: usize,
}
impl SourceLocation {
    pub fn new(lin: usize, col: usize) -> Self {
        SourceLocation {
            line: lin,
            column: col,
        }
    }
}

impl Display for SourceLocation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<{}:{}>", self.line, self.column)
    }
}

//令牌字符<开始位置,结束位置>;
#[derive(Debug, Clone, PartialEq)]
pub struct TextSpan {
    start: usize,
    end: usize,
    pub literal: String,
}

impl TextSpan {
    pub fn new(_start: usize, _length: usize, _literal: String) -> Self {
        Self {
            start: _start,
            end: _start + _length,
            literal: _literal,
        }
    }
    pub fn length(&self) -> usize {
        self.end - self.start
    }
}
