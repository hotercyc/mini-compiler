use std::time::SystemTime;

use super::{
    callable::SyntaxCallable, interpreter::Interpreter, object::Object, syntaxResult::SyntaxResult,
};

pub struct NativeClock {}

impl SyntaxCallable for NativeClock {
    fn Call(&self, _inter: &Interpreter, _args: Vec<Object>) -> Result<Object, SyntaxResult> {
        match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
            Ok(time) => Ok(Object::Number(time.as_secs_f64())),
            Err(time) => Err(SyntaxResult::SystemError(&format!(
                "Clock returned invalid duration:{:?}",
                time.duration()
            ))),
        }
    }

    fn arity(&self) -> usize {
        0
    }

    fn toString(&self) -> String {
        "Native:Clock".to_string()
    }
}
